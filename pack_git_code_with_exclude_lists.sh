#!/bin/bash

# 获取脚本所在目录
SCRIPT_DIR=$(dirname "$(realpath "$0")")
# 切换到脚本所在目录
cd "$SCRIPT_DIR" || exit
# 打印不带路径的脚本名称
SCRIPT_NAME=$(basename "$0")
# 你的脚本其他部分
echo ">>>>exec $(pwd)/$SCRIPT_NAME in dir '$(pwd)'"

Ver="0.1."
Postfix="_debug"
timestamp=$(date +%Y%m%d_%H%M%S)   # 获取当前时间的时间戳
filename="../git_vibration_calc_$ver${timestamp}_with_git_$Postfix.tar.gz"  # 生成带时间戳的文件名

echo $filename
tar czvf $filename $(find . -type f | grep -v -f packcode_exclude_lists.txt)