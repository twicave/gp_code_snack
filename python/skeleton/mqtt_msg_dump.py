import paho.mqtt.client as mqtt

# MQTT Broker 的地址和端口
broker_address = 127.0.0.1
broker_port = 1883
usr1=aaaa
pass1=aaaaaa

# 订阅的主题
topic = sensorshakeraw#

# 当接收到消息时的回调函数
def on_message(client, userdata, message)
    # 将二进制数据转换为十六进制并打印
    hex_data = message.payload.hex()
    print(Received message in hex, hex_data)
    exit();

# 创建 MQTT 客户端
client_id = aasdfads
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, client_id)

# 设置消息到达时的回调函数
client.on_message = on_message

# 连接到 MQTT Broker
client.username_pw_set(usr1, pass1)
client.connect(broker_address, broker_port, keepalive=60)

# 订阅主题
client.subscribe(topic)

# 开始循环，等待消息到达
client.loop_forever()
