#!/usr/bin/python3
#using whereis python3 to get the location of your python cmd.
# -*- coding: utf-8 -*-
# 获取当前脚本文件所在目录的父目录，并构建相对路径
import os
import sys
import json
import redis
import argparse
current_dir = os.path.dirname(os.path.abspath(__file__))
project_path = os.path.join(current_dir, '..')
sys.path.append(project_path)
sys.path.append(current_dir)

def load_json_from_file(file_path):
    # 打开文件并读取内容
    with open(file_path, 'r', encoding='utf-8') as file:
        json_str = file.read()
    
    # 将 JSON 字符串解析为 Python 对象
    data = json.loads(json_str)
    
    return data

def json2redis(data, root_name='cfg', redis_host='localhost', redis_port=6379):
    # 连接到 Redis 服务器
    r = redis.Redis(host='localhost', port=6379, decode_responses=True)
    
    # 定义一个递归函数来遍历数据并存储到 Redis
    def store_in_redis(prefix, data):
        if isinstance(data, dict):
            for key, value in data.items():
                new_key = f"{prefix}:{key}"
                store_in_redis(new_key, value)
        elif isinstance(data, list):
            for index, item in enumerate(data):
                new_key = f"{prefix}:{index}"
                store_in_redis(new_key, item)
        else:
            r.set(prefix, data)
    
    # 调用函数，传入根键名
    store_in_redis(f"{root_name}", data)
    
    # 示例：从 Redis 获取存储的数据并打印
    for key in r.keys(f"{root_name}:*"):
        print(f"{key}: {r.get(key)}")

def main():
    # 创建 ArgumentParser 对象
    parser = argparse.ArgumentParser(description='处理文件和选项')

    # 添加参数
    parser.add_argument('jsonfile', type=str, help='json_file_name')
    parser.add_argument('--redis_ip', type=str, default='127.0.0.1', help='redis server ip')
    parser.add_argument('--redis_port', type=int, default=6379, help='redis server port')
    parser.add_argument('--verbose', action='store_true', help='启用详细模式')

    # 解析命令行参数
    args = parser.parse_args()

    # 使用参数
    print(f'输入文件: {args.jsonfile}')
    print(f'输出文件: {args.redis_ip}')
    print(f'处理次数: {args.redis_port}')
    print(f'详细模式: {"启用" if args.verbose else "禁用"}')
    
    json_obj = load_json_from_file(args.jsonfile)
    filename_without_ext, _ = os.path.splitext(args.jsonfile)
    root = filename_without_ext
    root = root.replace("/", ":")
    root = root.replace("\\", ":")
    print(root)
    json2redis(json_obj, root, args.redis_ip, args.redis_port)
    

if __name__ == '__main__':
    main()
 