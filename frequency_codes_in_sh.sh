# 使用 $() 捕获命令输出
free_space=$(df --block-size=1 / | awk 'NR==2 {print $4}')
print $free_space

# 配置信息读取
# JSON 文件路径
json_file="/home/app/cfg/system.json"
# 读取 JSON 文件并提取字段
disk_percent=$(jq -r '.video_record.disk_percent' "$json_file")
VIDEO_FILE_PATH=$(jq -r '.video_record.video_file_path' "$json_file")