#在 Bash 中，你可以使用 jq 工具来处理 JSON 文件并提取树状目录结构中的某个项点的值。jq 是一个强大的命令行 JSON 处理工具。
<<'EOF'
假设你有一个 JSON 文件 data.json，内容如下：
{
  "folder1": {
    "subfolder1": {
      "file1": "value1"
    },
    "subfolder2": {
      "file2": "value2"
    }
  },
  "folder2": {
    "file3": "value3"
  }
}
EOF

#如果你想提取 folder1/subfolder1/file1 的值，可以使用以下命令：

jq '.folder1.subfolder1.file1' data.json
#这条命令会输出：
#"value1"

#如果你只想获取没有引号的值，可以使用 -r 选项：
jq -r '.folder1.subfolder1.file1' data.json
#这将输出：
#value1

#bash中的赋值
cfg1=$(jq -r '.video_record.disk_percent' "$json_file")

#Array元素访问： jq idx base0
#{
#  "array": [10, 20, 30, 40]
#}
#访问数组元素
#获取特定索引的元素
#要获取数组中索引为 1 的元素（即 20），可以使用以下命令： 
jq '.array[1]' data.json