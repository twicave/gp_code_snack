#!/bin/bash

# 获取传递的参数数量
num_args=$#

if [ $num_args -ne 1 ]; then
    echo "usage: $0 <chIdxbase1>"
    exit 1
else
    CHIDX_BASE1=$(printf "%d" "$1")
fi


# 变量
SERVICE_NAME="guide_ch_${CHIDX_BASE1}_recording"
echo "Now Watching service: $SERVICE_NAME"

# 获取当前时间的秒数
nowInSec=$(date +%s)

#得到下一个3600/4的以秒计算的位置:3600/4=900  3600=1hour, 3600/4=1quarter.
nextQuarterInSec=$(( ( (nowInSec + 899) / 900 ) * 900 ))
waitInSec=$((nextQuarterInSec - nowInSec))

#不再等待太短的时间
if [ $waitInSec -lt 60 ]; then
    waitInSec=$((waitInSec+900))
fi

# 计算需要等待的秒数
echo "等待 $waitInSec 秒"
sleep $waitInSec

#重启脚本
sudo systemctl restart "${SERVICE_NAME}" &
