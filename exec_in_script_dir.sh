#!/bin/bash

# 获取脚本所在目录
SCRIPT_DIR=$(dirname "$(realpath "$0")")

# 切换到脚本所在目录
cd "$SCRIPT_DIR" || exit

# 打印不带路径的脚本名称
SCRIPT_NAME=$(basename "$0")

# 你的脚本其他部分
echo ">>>>exec $(pwd)/$SCRIPT_NAME in dir '$(pwd)'"

